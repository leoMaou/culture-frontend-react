FROM node:10-slim

RUN mkdir /data

WORKDIR /data

COPY . /data/
RUN npm i --silent
RUN npm install serve -g
RUN npm run build

COPY docker-entrypoint.sh /docker-entrypoint.sh
CMD sh /docker-entrypoint.sh

EXPOSE 5000
