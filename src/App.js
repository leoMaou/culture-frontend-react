import React from 'react';
import './App.css';
import Obras from "./components/works/Obras";
import Usuarios from "./components/user/Usuarios";
import Autores from "./components/author/Autores";
import Personagens from "./components/character/Personagens";
import Dubladores from "./components/actor/Dubladores";
import Header from "./components/Header";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Container, Box } from "@material-ui/core";

function App() {
  return (

      <Box>
        <Header></Header>
        <Container>
          <BrowserRouter>
            <Switch>
              <Route exact path="/" component={Usuarios} />
              <Route path="/users" component={Usuarios} />
              <Route path="/works" component={Obras} />
              <Route path="/characters" component={Personagens} />
              <Route path="/authors" component={Autores} />
              <Route path="/actors" component={Dubladores} />
            </Switch>
          </BrowserRouter>
        </Container>
      </Box>);
}

export default App;
