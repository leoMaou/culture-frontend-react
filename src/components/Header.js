import React from 'react'
import {Button, Toolbar, AppBar, Typography} from '@material-ui/core'

export default class Header extends React.Component{
    render(){
        return(
        <AppBar position="static">
        <Toolbar>
            <Typography variant='h5'>Culture app</Typography>
            <Button color="inherit" href="/users">Usuários</Button>
            <Button color="inherit" href="/works">Obras</Button>
            <Button color="inherit" href="/characters">Personagens</Button>
            <Button color="inherit" href="/authors">Autores</Button>
            <Button color="inherit" href="/actors">Dubladores</Button>
            </Toolbar>
        </AppBar>
        )
    }
}