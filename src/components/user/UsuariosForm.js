import React from "react";
import {Button, TextField, FormControl, Box} from '@material-ui/core'

export default class UsuariosForm extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            nome: "",
            login: "",
            senha: "",
        }
    }

    handleChange = (event) => {
        event.preventDefault();

        this.setState({
            [event.target.id]: event.target.value
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log("cadastrando usuario");

        const usuario = {
            nome: this.state.nome,
            login: this.state.login,
            senha: this.state.senha,
        };

        this.props.handlePost(usuario);

        this.setState({
            nome: "",
            login: "",
            senha: "",
        });
    }

    render() {
        return (
            <Box display="flex" flexDirection='column'>
                <FormControl style={{margin: '3rem'}} onSubmit={this.handleSubmit}>
                    <TextField type="text" id="nome" placeholder="nome" value={this.state.nome} onChange={this.handleChange}/>
                    <TextField type="text" id="login" placeholder="login" value={this.state.login} onChange={this.handleChange}/>
                    <TextField type="password" id="senha" placeholder="senha" value={this.state.senha} onChange={this.handleChange}/>
                    <Button variant="contained"  color="primary" type="submit" onClick={this.handleSubmit}>Cadastrar novo usuario</Button>
                </FormControl>
            </Box>
        );
    }

}