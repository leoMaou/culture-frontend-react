import React from "react";
import Axios from 'axios'
import UsuariosForm from "./UsuariosForm";
import MaterialTable from 'material-table';
import Snackbar from "@material-ui/core/Snackbar";

export default class Usuarios extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            usuarios: [],
            selecionado: null
        }
    }

    componentDidMount = () => {
        this.handleGet()
    }

    handleGet = () => {
        const url = 'http://localhost:8080/usuarios/?limit=99';
        var request = Axios.get(url, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then((response) => {
            console.log(response.data);

            this.setState({
                usuarios: response.data
            });
        });
    }

    handlePost = (usuario) => {
        console.log("post")
        const url = 'http://localhost:8080/usuarios/salvar';
        var request = Axios.post(url, {"usuario": usuario});

        request.then((response) => {
            console.log(response);
            this.handleGet();
        }).catch((reason, response) => {
            this.setState({
                messageSnack: 'Não foi possivel salvar o usuario',
                open: true,
            })
            console.error(reason);
        });
    }

    handlePut = (id, usuario) => {
        const url = 'http://localhost:8080/usuarios/atualizar/' + id;
        var request = Axios.put(url, {"usuario": usuario}, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then((response) => {
            console.log(response);
            this.handleGet();
            this.setState({
                selecionado: null
            });
        });
    }

    handleSubmit = (usuario) => {
        if (this.state.selecionado == null) {
            this.handlePost(usuario);
        } else {
            this.handlePut(this.state.selecionado, usuario);
        }
    }

    handleDelete = (id) => {
        console.log("deletar: " + id);

        const url = 'http://localhost:8080/usuarios/' + id;
        var request = Axios.delete(url, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then((response) => {
            console.log(response);
            this.handleGet();
        });
    }

    handleSelect = (id) => {
        console.log("ide: " + id);
        this.setState({
            selecionado: id
        });
    }

    render() {
        /*var listaUsuarios = this.state.usuarios.map((item) => {
            return <div key={item._id}>{item.nome} {(item.login)}
                <button onClick={() => this.handleDelete(item._id)}>Deletar</button>
                <button onClick={() => this.handleSelect(item._id)}>Selecionar</button>
            </div>
        });*/

        const columns = [
            {title: 'Nome', field: 'nome'},
            {title: 'Login', field: 'login'},
            {title: 'Senha', field: 'senha'}
        ];

        return (<div>
            <link
                rel="stylesheet"
                href="https://fonts.googleapis.com/icon?family=Material+Icons"
            />
            <UsuariosForm handlePost={this.handleSubmit}></UsuariosForm>
            <MaterialTable
                title="Lista de Usuarios"
                columns={columns}
                data={this.state.usuarios}
                editable={{
                    onRowUpdate: (newData, oldData) =>
                        new Promise((resolve) => {
                            setTimeout(() => {
                                resolve();
                                this.handlePut(oldData._id, newData);
                                /*if (oldData) {
                                    this.setState((prevState) => {
                                        const data = [...prevState.usuarios];
                                        data[data.indexOf(oldData)] = newData;
                                        return { ...prevState, data };
                                    });
                                }*/
                            }, 600);
                        }),
                    onRowDelete: (oldData) =>
                        new Promise((resolve) => {
                            setTimeout(() => {
                                resolve();
                                this.handleDelete(oldData._id)
                            }, 600);
                        }),
                }}
            />
            <Snackbar
                anchorOrigin={{vertical: 'bottom', horizontal: 'center'}}
                open={this.state.open}
                onClose={this.handleClose}
                message={this.state.messageSnack}
            />
        </div>);
    }

}