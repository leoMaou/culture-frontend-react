import React from "react";
import {Button, TextField, FormControl, Box, TextareaAutosize} from '@material-ui/core'

export default class ObrasForm extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            nome: "",
            descricao: "",
            status: "",
            fotoUrl: "",
            source: "",
            formato: "",
        }
    }

    handleChange = (event) => {
        event.preventDefault();

        this.setState({
            [event.target.id]: event.target.value
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const obra = {
            nome: this.state.nome,
            descricao: this.state.descricao,
            status: this.state.status,
            fotoUrl: this.state.fotoUrl,
            source: this.state.source,
            formato: this.state.formato
        };

        this.props.handlePost(obra);

        this.setState({
            nome: "",
            descricao: "",
            status: "",
            fotoUrl: "",
            source: "",
            formato: "",
        });
    }

    render() {
        return (
            <Box  display="flex" flexDirection='column'>
                <FormControl style={{margin: '3rem'}} onSubmit={this.handleSubmit}>
                    <TextField type="text" id="nome" placeholder="nome" value={this.state.nome} onChange={this.handleChange}></TextField>
                    <TextField type="text" id="status" placeholder="status" value={this.state.status} onChange={this.handleChange}></TextField>
                    <TextField type="text" id="fotoUrl" placeholder="fotoUrl" value={this.state.fotoUrl} onChange={this.handleChange}></TextField>
                    <TextField type="text" id="source" placeholder="source"  value={this.state.source} onChange={this.handleChange}></TextField>
                    <TextField type="text" id="formato" placeholder="formato" value={this.state.formato} onChange={this.handleChange}></TextField>
                    <TextareaAutosize id="descricao" aria-label="minimum height" rowsMin={4} placeholder="descrição" value={this.state.descricao}  onChange={this.handleChange}/>
                    <Button variant="contained"  color="primary" type="submit" onClick={this.handleSubmit}>Cadastrar</Button>
                </FormControl>
            </Box>
        );
    }

}