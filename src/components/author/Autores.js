import React from "react";
import Axios from 'axios'
import AutoresForm from "./AutoresForm";
import {
    List,
    ListItem,
    Avatar,
    ListItemAvatar,
    ListItemText,
    Typography,
    ListItemSecondaryAction,
    IconButton,
    Divider, TextField, Box
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Snackbar from "@material-ui/core/Snackbar";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";

export default class Autores extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            autores: [],
            selecionado: null,
            searchName: "",
            open: false,
            messageSnack: 'Operação realizado com sucesso!'
        }
    }

    componentDidMount = () => {
        this.handleGet()
    }

    handleGet = () => {

        var query = "";

        if (this.state.searchName !== "") {
            query += "nome=" + this.state.searchName;
        }

        const url = 'http://localhost:8080/autores/?' + query;
        var request = Axios.get(url, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then( (response) => {
            console.log(response.data);

            this.setState({
                autores: response.data
            });
        });
    }

    handlePost = (autor) => {
        const url = 'http://localhost:8080/autores/salvar';
        var request = Axios.post(url, {"autor": autor},{
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then((response)=> {
            console.log(response);
            this.handleGet();
        }).catch((reason, response) => {
            this.setState({
                messageSnack: 'Não foi possivel salvar o Autor',
                open: true,
            })
            console.error(reason);
        });
    }

    handlePut = (id, autor) => {
        const url = 'http://localhost:8080/autores/atualizar/' + id;
        var request = Axios.put(url, {"autor": autor},{
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then((response)=> {
            console.log(response);
            this.handleGet();
            this.setState({
                selecionado: null
            });
        });
    }

    handleSubmit = (autor) => {
        if(this.state.selecionado == null) {
            this.handlePost(autor);
        }else {
            this.handlePut(this.state.selecionado, autor);
        }
    }

    handleDelete = (id) => {
        console.log("deletar: " + id);

        const url = 'http://localhost:8080/autores/' + id;
        var request = Axios.delete(url, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then( (response) => {
            console.log(response);
            this.handleGet();
        });
    }

    handleSelect = (id) => {
        console.log("ide: " + id);
        this.setState({
           selecionado: id
        });
    }

    handleChangeSearch = (event) => {
        event.preventDefault();

        this.setState({
            [event.target.id]: event.target.value
        })
    }

    handleClose = () => {
        this.setState({
            open: false
        })
    }

    render() {

        const listaAutores = this.state.autores.map((item) => {
            return <div key={item._id}>
                <ListItem alignItems="flex-start">
                    <ListItemAvatar>
                        <Avatar alt="photo" src={item.fotoUrl}/>
                    </ListItemAvatar>
                    <ListItemText
                        primary={item.nome}
                        secondary={
                            <React.Fragment>
                                <Typography
                                    component="span"
                                    variant="body2"
                                    color="textPrimary"
                                    style={{paddingInlineEnd: 30}}
                                >
                                    {item.genero}
                                </Typography>
                            </React.Fragment>
                        }
                    />
                    <ListItemSecondaryAction>
                        <IconButton edge="end" aria-label="edit"  onClick={() => this.handleSelect(item._id)}>
                            <EditIcon/>
                        </IconButton>
                        <IconButton edge="end" aria-label="delete" onClick={() => this.handleDelete(item._id)}>
                            <DeleteIcon/>
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
                <Divider variant="inset" component="li" />
            </div>
        });

        return (<div>
            <AutoresForm handlePost={this.handleSubmit}></AutoresForm>
            <Box display="flex" flexDirection='column'>
                <TextField
                    id="searchName"
                    label="Pesquisar pelo Nome"
                    onChange={this.handleChangeSearch}
                    onKeyPress={event => {
                        if (event.key === 'Enter') {
                            this.handleGet();
                        }
                    }}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment>
                                <IconButton onClick={this.handleGet}>
                                    <SearchIcon/>
                                </IconButton>
                            </InputAdornment>
                        )
                    }}/>
            </Box>
            <h1>Lista de Autores</h1>
            <List>
                {listaAutores}
            </List>
            <Snackbar
                anchorOrigin={{vertical: 'bottom', horizontal: 'center'}}
                open={this.state.open}
                onClose={this.handleClose}
                message={this.state.messageSnack}
            />
        </div>);
    }

}