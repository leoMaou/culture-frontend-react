import React from "react";
import {Button, TextField, FormControl, Box} from '@material-ui/core'

export default class AutoresForm extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            nome: "",
            fotoUrl: "",
            genero: "",
        }
    }

    handleChange = (event) => {
        event.preventDefault();

        this.setState({
            [event.target.id]: event.target.value
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const autor = {
            nome: this.state.nome,
            fotoUrl: this.state.fotoUrl,
            genero: this.state.genero,
        };

        this.props.handlePost(autor);
        this.setState(
            {
                nome: "",
                fotoUrl: "",
                genero: "",
            }
        );
    }

    render() {
        return (
            <Box display="flex" flexDirection='column'>
                <FormControl style={{margin: '3rem'}} onSubmit={this.handleSubmit}>
                    <TextField type="text" id="nome" placeholder="nome" value={this.state.nome} onChange={this.handleChange}></TextField>
                    <TextField type="text" id="fotoUrl" placeholder="fotoUrl" value={this.state.fotoUrl} onChange={this.handleChange}></TextField>
                    <TextField type="text" id="genero" placeholder="genero"  value={this.state.genero} onChange={this.handleChange}></TextField>
                    <Button variant="contained"  color="primary" type="submit" onClick={this.handleSubmit}>Cadastrar</Button>
                </FormControl>
            </Box>
        );
    }

}