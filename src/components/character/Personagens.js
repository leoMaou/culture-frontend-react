import React from "react";
import Axios from 'axios'
import PersonagensForm from "./PersonagensForm";
import {
    List,
    ListItem,
    Avatar,
    ListItemAvatar,
    ListItemText,
    Typography,
    ListItemSecondaryAction,
    IconButton,
    Divider, TextField, Box
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Snackbar from "@material-ui/core/Snackbar";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";


export default class Personagens extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            personagens: [],
            selecionado: null,
            searchName: "",
            open: false,
            messageSnack: 'Operação realizado com sucesso!'
        }
    }

    componentDidMount = () => {
        this.handleGet()
    }

    handleGet = () => {

        var query = "";

        if (this.state.searchName !== "") {
            query += "&nome=" + this.state.searchName;
        }

        const url = 'http://localhost:8080/personagens/?skip=0&limit=99' + query;
        var request = Axios.get(url, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then((response) => {
            console.log(response.data);

            this.setState({
                personagens: response.data
            });
        });
    }

    handlePost = (personagem) => {
        const url = 'http://localhost:8080/personagens/salvar';
        var request = Axios.post(url, {"personagem": personagem}, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then((response) => {
            console.log(response);
            this.handleGet();
        }).catch((reason, response) => {
            this.setState({
                messageSnack: 'Não foi possivel salvar o personagem',
                open: true,
            })
            console.error(reason);
        });
    }

    handlePut = (id, personagem) => {
        const url = 'http://localhost:8080/personagens/atualizar/' + id;
        var request = Axios.put(url, {"personagem": personagem}, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then((response) => {
            console.log(response);
            this.handleGet();
            this.setState({
                selecionado: null
            });
        });
    }

    handleSubmit = (personagem) => {
        if (this.state.selecionado == null) {
            this.handlePost(personagem);
        } else {
            this.handlePut(this.state.selecionado, personagem);
        }
    }

    handleDelete = (id) => {
        console.log("deletar: " + id);

        const url = 'http://localhost:8080/personagens/' + id;
        var request = Axios.delete(url, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then((response) => {
            console.log(response);
            this.handleGet();
        });
    }

    handleSelect = (id) => {
        console.log("ide: " + id);
        this.setState({
            selecionado: id
        });
    }

    handleChangeSearch = (event) => {
        event.preventDefault();

        this.setState({
            [event.target.id]: event.target.value
        })
    }

    handleClose = () => {
        this.setState({
            open: false
        })
    }

    render() {

        var listaPersonagens = this.state.personagens.map((item) => {
            return <div key={item._id}>
                <ListItem alignItems="flex-start">
                    <ListItemAvatar>
                        <Avatar alt="character char" src={item.fotoUrl}/>
                    </ListItemAvatar>
                    <ListItemText
                        primary={item.nome}
                        secondary={
                            <React.Fragment>
                                <Typography
                                    component="span"
                                    variant="body2"
                                    color="textPrimary"
                                    style={{paddingInlineEnd: 30}}>
                                    {item.descricao}
                                </Typography>
                            </React.Fragment>
                        }
                    />
                    <ListItemSecondaryAction>
                        <IconButton edge="end" aria-label="edit"  onClick={() => this.handleSelect(item._id)}>
                            <EditIcon/>
                        </IconButton>
                        <IconButton edge="end" aria-label="delete" onClick={() => this.handleDelete(item._id)}>
                            <DeleteIcon/>
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
                <Divider variant="inset" component="li" />
            </div>
        });

        return (<div>
            <PersonagensForm handlePost={this.handleSubmit}></PersonagensForm>
            <section>
                <Box display="flex" flexDirection='column'>
                    <TextField
                        id="searchName"
                        label="Pesquisar pelo Nome"
                        onChange={this.handleChangeSearch}
                        onKeyPress={event => {
                            if (event.key === 'Enter') {
                                this.handleGet();
                            }
                        }}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment>
                                    <IconButton onClick={this.handleGet}>
                                        <SearchIcon/>
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}/>
                </Box>
                <h1>Lista de personagens</h1>
                <List>
                    {listaPersonagens}
                </List>
                <Snackbar
                    anchorOrigin={{vertical: 'bottom', horizontal: 'center'}}
                    open={this.state.open}
                    onClose={this.handleClose}
                    message={this.state.messageSnack}
                />
            </section>
        </div>);
    }

}