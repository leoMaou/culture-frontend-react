import React from "react";
import {Button, TextField, FormControl, Box, TextareaAutosize} from '@material-ui/core'

export default class PersonagensForm extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            nome: "",
            descricao: "",
            genero: "",
            fotoUrl: ""
        }
    }

    handleChange = (event) => {
        event.preventDefault();

        this.setState({
            [event.target.id]: event.target.value
        })
    }

    handleSubmit = (event) => {
        console.log("Handle submit")
        event.preventDefault();

        const personagem = {
            nome: this.state.nome,
            descricao: this.state.descricao,
            genero: this.state.genero,
            fotoUrl: this.state.fotoUrl
        };

        this.props.handlePost(personagem);

        this.setState({
            nome: "",
            descricao: "",
            genero: "",
            fotoUrl: ""})
    }

    render() {
        return (
            <Box display="flex" flexDirection='column'>
                <FormControl style={{margin: '3rem'}} onSubmit={this.handleSubmit}>
                    <TextField type="text" id="nome" placeholder="nome" value={this.state.nome} onChange={this.handleChange}></TextField>
                    <TextField type="text" id="genero" placeholder="genero" value={this.state.genero} onChange={this.handleChange}></TextField>
                    <TextField type="text" id="fotoUrl" placeholder="fotoUrl" value={this.state.fotoUrl} onChange={this.handleChange}></TextField>
                    <TextareaAutosize id="descricao" aria-label="minimum height" rowsMin={4} placeholder="descrição" value={this.state.descricao}  onChange={this.handleChange}/>
                    <Button variant="contained"  color="primary" type="submit" onClick={this.handleSubmit}>Cadastrar</Button>
                </FormControl>
            </Box>
        );
    }

}