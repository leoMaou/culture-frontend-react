import React from "react";
import {Button, TextField, FormControl, Box, TextareaAutosize} from '@material-ui/core'

export default class DubladoresForm extends React.Component {

    constructor(props) {
        super(props);

        console.log(this.props)

        this.state = {
            nome: "",
            descricao: "",
            fotoUrl: "",
            genero: "",
        }
    }

    handleChange = (event) => {
        event.preventDefault();

        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const autor = {
            nome: this.state.nome,
            fotoUrl: this.state.fotoUrl,
            descricao: this.state.descricao,
            genero: this.state.genero,
        };

        this.props.handlePost(autor);
        this.setState({
            nome: "",
            descricao: "",
            fotoUrl: "",
            genero: "",
        });
    }

    render() {
        return (
            <Box display="flex" flexDirection='column'>
                <FormControl style={{margin: '3rem'}} onSubmit={this.handleSubmit}>
                    <TextField type="text" id="nome" placeholder="nome" value={this.state.nome}
                               onChange={this.handleChange}/>
                    <TextField type="text" id="genero" placeholder="genero" value={this.state.genero}
                               onChange={this.handleChange}/>
                    <TextField type="text" id="fotoUrl" placeholder="fotoUrl" value={this.state.fotoUrl}
                               onChange={this.handleChange}/>
                    <TextareaAutosize id="descricao" aria-label="minimum height" rowsMin={4} placeholder="descrição" value={this.state.descricao}  onChange={this.handleChange}/>
                    <Button variant="contained"  color="primary" type="submit" onClick={this.handleSubmit}>Cadastrar</Button>
                </FormControl>
            </Box>);
    }

}