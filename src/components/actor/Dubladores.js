import React from "react";
import Axios from 'axios'
import DubladoresForm from "./DubladoresForm";
import {
    List,
    ListItem,
    Avatar,
    ListItemAvatar,
    ListItemText,
    Typography,
    ListItemSecondaryAction,
    IconButton,
    Divider, TextField, Box
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Snackbar from "@material-ui/core/Snackbar";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";

export default class Dubladores extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            dubladores: [],
            selecionado: null,
            searchName: "",
            open: false,
            messageSnack: 'Operação realizado com sucesso!'
        }
    }

    componentDidMount = () => {
        this.handleGet()
    }

    handleGet = () => {

        var query = "";

        if (this.state.searchName !== "") {
            query += "nome=" + this.state.searchName;
        }

        const url = 'http://localhost:8080/dubladores/?' + query;
        var request = Axios.get(url, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then((response) => {
            console.log(response.data);

            this.setState({
                dubladores: response.data
            });
        });
    }

    handlePost = (dublador) => {
        const url = 'http://localhost:8080/dubladores/salvar';
        var request = Axios.post(url, {"dublador": dublador}, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then((response) => {
            console.log(response);
            this.handleGet();
            this.setState({
                selecionado: null
            });
        }).catch((reason, response) => {
            this.setState({
                messageSnack: 'Não foi possivel salvar o Dublador',
                open: true,
            });
            console.error(reason);
        });
    }

    handlePut = (id, dublador) => {
        const url = 'http://localhost:8080/dubladores/atualizar/' + id;
        var request = Axios.put(url, {"dublador": dublador}, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then((response) => {
            console.log(response);
            this.handleGet();
            this.setState({
                selecionado: null
            });
        });
    }

    handleSubmit = (dublador) => {
        if (this.state.selecionado == null) {
            this.handlePost(dublador);
        } else {
            this.handlePut(this.state.selecionado, dublador);
        }
    }

    handleDelete = (id) => {
        console.log("deletar: " + id);

        const url = 'http://localhost:8080/dubladores/' + id;
        var request = Axios.delete(url, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6ImxlbyIsInNlbmhhIjoiMSIsImlhdCI6MTU5MzUyOTg5MiwiZXhwIjoxNjI1MDY1ODkyfQ.1yecCwetVjBL7iyqgGDwbv9B0GLRgL4dvTjR9sfLT8M",
            }
        });

        request.then((response) => {
            console.log(response);
            this.handleGet();
        });
    }

    handleSelect = (id) => {
        console.log("ide: " + id);
        this.setState({
            selecionado: id
        });
    }

    handleChangeSearch = (event) => {
        event.preventDefault();

        this.setState({
            [event.target.id]: event.target.value
        })
    }

    handleClose = () => {
        this.setState({
            open: false
        });
    }

    render() {

        const listaDubladores = this.state.dubladores.map((item) => {
            return <div key={item._id}>
                <ListItem alignItems="flex-start">
                    <ListItemAvatar>
                        <Avatar alt="photo" src={item.fotoUrl}/>
                    </ListItemAvatar>
                    <ListItemText
                        primary={item.nome}
                        secondary={
                            <React.Fragment>
                                <Typography
                                    component="span"
                                    variant="body2"
                                    color="textPrimary"
                                    style={{paddingInlineEnd: 30}}
                                >
                                    {item.genero} {item.descricao}
                                </Typography>
                            </React.Fragment>
                        }
                    />
                    <ListItemSecondaryAction>
                        <IconButton edge="end" aria-label="edit" onClick={() => this.handleSelect(item._id)}>
                            <EditIcon/>
                        </IconButton>
                        <IconButton edge="end" aria-label="delete" onClick={() => this.handleDelete(item._id)}>
                            <DeleteIcon/>
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
                <Divider variant="inset" component="li"/>
            </div>
        });

        return (<div>
            <DubladoresForm handlePost={this.handleSubmit}></DubladoresForm>
            <Box display="flex" flexDirection='column'>
                <TextField
                    id="searchName"
                    label="Pesquisar pelo Nome"
                    onChange={this.handleChangeSearch}
                    onKeyPress={event => {
                        if (event.key === 'Enter') {
                            this.handleGet();
                        }
                    }}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment>
                                <IconButton onClick={this.handleGet}>
                                    <SearchIcon/>
                                </IconButton>
                            </InputAdornment>
                        )
                    }}/>
            </Box>
            <h1>Lista de Dubladores</h1>
            <List>
                {listaDubladores}
            </List>
            <Snackbar
                anchorOrigin={{vertical: 'bottom', horizontal: 'center'}}
                open={this.state.open}
                onClose={this.handleClose}
                message={this.state.messageSnack}
            />
        </div>);
    }

}