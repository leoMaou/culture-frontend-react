#!/bin/sh

echo "----------------------------------------"
echo "Running Docker Entrypoint"
echo "----------------------------------------"
echo "Expose port: 5000"
echo "Local date UTC: " `date -u`
echo "Local date: " `date`

echo "----------------------------------------"

echo "Starting application"
echo "----------------------------------------"

cd /data && serve -s build
